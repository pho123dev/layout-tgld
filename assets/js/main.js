(function (jQuery) {
    jQuery(window).resize(function () {
        if (jQuery(window).width() >= 1200) {
            jQuery("body").removeClass("opened-nav-animate");
            jQuery("#hamburger-icon").removeClass("active");
        }
        if (jQuery(window).width() <= 1000) {}
    });

    function backtotop() {
        var i = jQuery(window).outerHeight() * 2;
        jQuery(window).scroll(function () {
            if (jQuery(window).scrollTop() > i) {
                jQuery(".m-backtotop").addClass("active")
            } else {
                jQuery(".m-backtotop").removeClass("active")
            }
        });
        jQuery(".m-backtotop").click(function () {
            jQuery("html, body").animate({
                    scrollTop: 0
                },
                "slow");
            return false
        })
    }

    function MegaMenu() {
        var body = jQuery("body");
        var IdMainNavMobile = jQuery("#main-nav-mobile");
        var IdMainNavTool = jQuery("#main-nav-tool");
        var ClassClose = jQuery(".menu_close");
        var IdIcon = jQuery("#hamburger-icon");
        var ClassOverlay = jQuery(".menu_overlay");
        if (IdMainNavTool.length) {
            jQuery("> ul", IdMainNavMobile).prepend(jQuery("> li", IdMainNavTool).clone())
        }
        jQuery("> ul", IdMainNavMobile).prepend(jQuery("#main-navigation > li").clone());
        jQuery("ul.sub-menu, .pho-megamenu-wrapper, ul.navigation-mobile").each(function () {
            jQuery(this).parent(".menu-item-has-children").prepend('<span class="open-submenu"></span>')
        });

        jQuery(".open-submenu").on("click", function (l) {
            l.stopPropagation();
            l.preventDefault();
            jQuery(this).closest("li").toggleClass("active-menu-item");
            jQuery(this).closest("li").children(".sub-menu, .pho-megamenu-wrapper").slideToggle()
        });
        jQuery(IdIcon).click(function () {
            jQuery(IdMainNavMobile).animate({
                left: "5%"
            }, 500);
            jQuery("html").addClass("menu-open");
            jQuery(ClassOverlay).addClass("hidden_menu");
        });
        jQuery(ClassClose).click(function () {
            jQuery(IdMainNavMobile).animate({
                left: "-100%"
            }, 500);
            jQuery(ClassOverlay).removeClass("hidden_menu");
            jQuery("html").removeClass("menu-open");
        });
        jQuery(ClassOverlay).click(function () {
            jQuery(IdMainNavMobile).animate({
                left: "-100%"
            }, 500);
            jQuery(ClassOverlay).removeClass("hidden_menu");
            jQuery("html").removeClass("menu-open");
        });
        jQuery(window).resize(function () {
            var l = jQuery(window).height() - jQuery("#header-content-mobile").height();
            if (jQuery("#wpadminbar").length) {
                l = l - jQuery("#wpadminbar").height()
            }
            IdMainNavMobile.css({
                "max-height": l
            })
        })
    }

    function stickyheader() {
        var $window = jQuery(window);  
        var header = jQuery(".sticky-header");
        $window.scroll(function () {
            if ($window.scrollTop() > 0) {
                header.addClass("fix");
                header.addClass("sscrollDown");
            } else {
                header.removeClass("fix");
                header.removeClass("sscrollDown");
            }
        })
    }
    function search() {
        jQuery('.header-search-wrapper .search-main').click(function () {
            jQuery('.search-form-main').toggleClass('active-search');
            jQuery('.search-form-main .search-field').focus();
        });
    }
    backtotop();
    search();
    stickyheader();
    MegaMenu();
})(jQuery);


// Form booking & call Popup
(function () {
    const popup = document.querySelector(".popup");
    const popupItems = document.querySelectorAll(".popup__item");
    const popupWrap = document.querySelector(".popup__wrap");
    const popupOverlay = document.querySelector(".popup__overlay");
    const popupClose = document.querySelector(".popup__close");
    const btnsPopup = document.querySelectorAll(".btn-popup");

    btnsPopup.forEach((btn) => {
        btn.addEventListener("click", () => {
            showPopup(btn.getAttribute("data-popup"));
        });
    });

    const showPopup = (index) => {
        popupItems.forEach((popup) => {
            popup.classList.remove("show");
        });
        popup.style.display = "block";
        document
            .querySelector(`.popup__item[data-popup="${index}"]`)
            .classList.add("show");

        popupWrap.classList = `popup__wrap popup__wrap--${index}`;
        setTimeout(() => {
            popup.classList.add("show");
        }, 10);
    };


    const closePopup = () => {
        popup.classList.remove("show");
        setTimeout(() => {
            popup.style.display = "";
            popupWrap.classList = `popup__wrap`;
        }, 500);
    };

    popupOverlay.addEventListener("click", closePopup);
    popupClose.addEventListener("click", closePopup);
})();