jQuery(document).ready(function () {
    var sidebar = jQuery(".ScrollSticky");
    var $window = jQuery(window);
    var sidebarHeight = sidebar.innerHeight();
    var footerOffsetTop = jQuery("#footer").offset().top;
    var sidebarOffset = sidebar.offset();
    $window.scroll(function () {
      if ($window.scrollTop() > sidebarOffset.top) {
        sidebar.addClass("fix");
      } else {
        sidebar.removeClass("fix");
      }
      if ($window.scrollTop() + sidebarHeight > footerOffsetTop) {
        console.log(footerOffsetTop);
        sidebar.css({
          "top": -($window.scrollTop() + sidebarHeight - footerOffsetTop)
        });
      } else {
        sidebar.css({
          "top": "40px"
        });
      }
    });
  });