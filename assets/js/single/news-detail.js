jQuery(document).ready(function () {
  jQuery("#menu-sidebar .menu-item-has-children").append("<i class='gg-chevron-down'></i>");
  jQuery("#menu-sidebar i").on("click", function (a) {
    a.preventDefault();
    jQuery("#menu-sidebar > ul > li > ul").toggle('fast');
    jQuery("#menu-sidebar > ul > li").children('i').toggleClass('gg-chevron-down gg-chevron-up');
  });
  var sidebar = jQuery(".ScrollSticky");
  var $window = jQuery(window);
  var sidebarHeight = sidebar.innerHeight();
  var footerOffsetTop = jQuery("#footer").offset().top;
  var sidebarOffset = sidebar.offset();
  $window.scroll(function () {
    if ($window.scrollTop() > sidebarOffset.top) {
      sidebar.addClass("fix");
    } else {
      sidebar.removeClass("fix");
    }
    if ($window.scrollTop() + sidebarHeight > footerOffsetTop) {
      sidebar.css({
        "top": -($window.scrollTop() + sidebarHeight - footerOffsetTop)
      });
    } else {
      sidebar.css({
        "top": "90px"
      });
    }
  });
});