function openCity(d, e) {
    var b, c, a;
    c = document.getElementsByClassName("tabcontent");
    for (b = 0; b < c.length; b++) {
        c[b].style.display = "none"
    }
    a = document.getElementsByClassName("tablinks");
    for (b = 0; b < a.length; b++) {
        a[b].className = a[b].className.replace(" active", "")
    }
    document.getElementById(e).style.display = "block";
    d.currentTarget.className += " active"
}
(function(jQuery) {
    function owl($slider) {
        jQuery($slider).owlCarousel({
            autoplay: false,
            lazyLoad: true,
            loop: true,
            autoplayTimeout: 4000,
            smartSpeed: 400,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 2,
                    center: true,
                    nav: true,
                    margin: 5
                },
                600: {
                    items: 4,
                    center: true,
                    nav: true,
                    margin: 5
                }
            }
        });
    }
    owl(".slider-center-1");
    owl(".slider-center-2");
    owl(".slider-center-3");
})(jQuery);